import moment from 'moment-timezone'
import * as cron from 'node-cron'
import Parser from 'rss-parser'
import { bot } from './commands.js'
import { StateHandler } from './state.js'
import { BikeResponse, BikeSubscription, DocumentResponse, State } from './types.js'
import { fetchBikes, fetchWikiDocument, getOption, sendLiveLocation, sendMessage } from './utils.js'

moment.tz.setDefault('Europe/Berlin')
moment.locale('de-DE')

let mentionCooldown: moment.Moment = moment()
let deadTrackerCooldown: moment.Moment = moment()

const parser: Parser = new Parser({
  headers: {
    Authorization: 'Token ' + process.env.ESEL_TOKEN,
    Accept: 'application/json'
  }
})

let lastUpdate: moment.Moment = moment()
const stateHandler: StateHandler = new StateHandler()

const doUpdate = async () => {
  const feed: Record<string, unknown> & Parser.Output<Record<string, unknown>> = await parser.parseURL(
    process.env.ENDPOINT + 'maintenance/logentryfeed?format=json'
  )
  const thresholdGeofenceItems: (Record<string, unknown> & Parser.Item)[] = feed.items.filter(e =>
    ((e.content ?? '').includes('moved above threshold without Rent') || (e.content ?? '').includes('geofence')) &&
    moment(e.isoDate).isAfter(lastUpdate))
  const autoFinishRentItems: (Record<string, unknown> & Parser.Item)[] = feed.items
    .filter(e => (e.content ?? '').includes('has been auto finished'))

  if (thresholdGeofenceItems.length > 0) {
    // Obtain a copy of all bike locations, we're gonna need it later
    const bikes: BikeResponse[] = await fetchBikes(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '')

    thresholdGeofenceItems
      .map(e => e.content)
      .forEach(async msg => {
        if (msg !== undefined) {
          const bikeId: string | undefined = (msg.match(/\d{4}/g) ?? [])[0]
          const bikePK: RegExpMatchArray | null = msg.match(/\/admin\/bikesharing\/bike\/(\d*)/)
          let bikeLocsLink = ''
          if (bikePK != null) {
            bikeLocsLink =
              '\nhttps://di.esel.ac/admin/bikesharing/location/?bike__id__exact=' +
              bikePK[1] +
              '&o=-5'
          }
          await sendMessage(process.env.CHAT_ID ?? '', msg + bikeLocsLink, {
            parse_mode: 'HTML'
          })
          if (bikeId !== undefined) {
            // Look up the location of the bike and send a telegram location along with the message
            const loc: [number, number] = bikes
              .filter(e => e.bike_number === bikeId)
              .map(e => [e.trackers[0].lat, e.trackers[0].lng])[0] as [number, number]

            await bot.sendLocation(process.env.CHAT_ID ?? '', ...loc)
          }
        }
      })
  }

  if (autoFinishRentItems.length > 0) {
    autoFinishRentItems
      .map(e => e.content)
      .forEach(async msg => {
        if (msg !== undefined) {
          const bikeId: string | undefined = (msg.match(/\d{4}/g) ?? [])[0]

          if (bikeId !== undefined && await stateHandler.rentExists(bikeId)) {
            await stateHandler.removeRent(bikeId)
          }
        }
      })
  }

  lastUpdate = thresholdGeofenceItems
    .map(e => moment(e.isoDate))
    .reduce((a, b) => {
      return a.isBefore(b) ? b : a
    }, lastUpdate) // Give maximum or keep current last update
}

const doBikesCheck = async () => {
  let msg = ''
  const bikeInspections: { bike: string, time: number }[] = []

  let bikes: BikeResponse[] = await fetchBikes(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '')

  bikes = bikes
    .filter(bike => bike.wiki_link != null && bike.wiki_link !== '' && bike.availability_status === 'available')

  for (const b of bikes) {
    if (b.wiki_link != null) {
      const baseurl: string = b.wiki_link.split('//')[1].split('/')[0]
      let date: moment.Moment = moment()

      const document: DocumentResponse = await fetchWikiDocument(baseurl, process.env.WIKI_TOKEN ?? '', b.wiki_link.split('/')[b.wiki_link.split('/').length - 1])

      if (document.data != null) {
        const inspection: string[] = getOption('inspection', document.data.text).split('.')

        const day: number = parseInt(inspection[0])
        const month: number = parseInt(inspection[1])
        const year: number = inspection[2].length === 2 ? parseInt(inspection[2]) + 2000 : parseInt(inspection[2])

        date = moment([year, month - 1, day])
      }

      const lastInspectionTimeDiff: number = moment().diff(date, 'days')

      if (lastInspectionTimeDiff >= Number(process.env.BIKE_INSPECTION_THRESHOLD)) {
        bikeInspections.push({ bike: b.bike_number, time: lastInspectionTimeDiff })
      }
    }
  }

  if (bikeInspections.length > 0) {
    msg = 'Die folgenden Esel wurden schon seit *' + process.env.BIKE_INSPECTION_THRESHOLD + '* Tagen nicht mehr inspiziert:\n'
  }

  bikeInspections.forEach((e) => {
    msg += '- *' + e.bike + ':* Letzte Inspektion vor ' + e.time + ' Tagen\n'
  })

  if (bikeInspections.length > 0) {
    await sendMessage(process.env.CHANNEL_ID ?? '', msg, { parse_mode: 'Markdown' })

    bikeInspections.forEach(async (e) => {
      const bike = bikes.find((b) => b.bike_number === e.bike)

      if (bike !== undefined) {
        await bot.sendLocation(process.env.CHANNEL_ID ?? '', bike.trackers[0].lat, bike.trackers[0].lng)
      }
    })
  }
}

const doAllTrackerOfflineCheck = async (): Promise<void> => {
  if (moment().diff(mentionCooldown, 'hours') >= 2) {
    let bikes: BikeResponse[] = await fetchBikes(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '')

    bikes = bikes.filter((b) => b.trackers.length !== 0)

    if (bikes.every((b) => moment().diff(moment(b.trackers[0].last_reported), 'minutes') > Number(process.env.TRACKER_OFFLINE_THRESHOLD_MINUTES))) {
      const users: string = (process.env.USERS_TO_MENTION ?? '').split(',').map((u) => '@' + u.trim()).join(' ')

      await sendMessage(process.env.CHAT_ID ?? '', users + ' Tracker down!')
      mentionCooldown = moment()
    }
  }
}

const doTrackerDeadAndSubscriptionsCheck = async (): Promise<void> => {
  const currState: State = await stateHandler.getState()

  if (moment().diff(deadTrackerCooldown, 'hours') >= 2) {
    let bikes: BikeResponse[] = await fetchBikes(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '')

    bikes = bikes.filter((b) => b.trackers.length !== 0 && b.availability_status === 'available' && b.state === 'usable')

    bikes.forEach(async b => {
      if (moment().diff(moment(b.trackers[0].last_reported), 'hours') >= 24) { // 24h offline
        deadTrackerCooldown = moment()
        await sendMessage(process.env.CHAT_ID ?? '', `Das Bike mit der Nummer ${b.bike_number} ist online, aber hat seit über einen Tag kein Tracker Update mehr gesendet!`)
      }
    })
  }

  if (currState.subscriptions.length !== 0) {
    const bikes: BikeResponse[] = await fetchBikes(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '')
    const bikeSubscriptions: BikeSubscription[] = currState.subscriptions

    for (const sub of bikeSubscriptions) {
      const bike: BikeResponse = bikes.filter(e => Number(e.bike_number) === sub.id)[0]

      if (sub.lat !== bike.trackers[0].lat && sub.lng !== bike.trackers[0].lng) {
        await stateHandler.updateSubscription(sub.id, bike.trackers[0].lat, bike.trackers[0].lng)

        await sendMessage(process.env.CHAT_ID ?? '', `Neue Position von Bike ${sub.id}:\n(${moment(bike.trackers[0].last_reported).format('HH:mm:ss – DD.MM.YY (zz)')})`)
        await bot.sendLocation(process.env.CHAT_ID ?? '', bike.trackers[0].lat, bike.trackers[0].lng)
      }
    }
  }
}

await stateHandler.getState()

if (await stateHandler.getLiveLocation() !== null) {
  const interval = Number(setInterval(sendLiveLocation, 60 * 1000, Number((await stateHandler.getLiveLocation())?.bikeNumber), false))

  await sendLiveLocation(Number((await stateHandler.getLiveLocation())?.bikeNumber), false, interval)
}

bot.on('polling_error', (err) => console.error(moment().format('YYYY-MM-DD - HH:mm:ss') + ': ', err.name, err.message, err.cause))

cron.schedule('0 12 * * *', async () => { await doBikesCheck() }, { runOnInit: false, timezone: 'Europe/Berlin' })

await doUpdate()
await doAllTrackerOfflineCheck()
await doTrackerDeadAndSubscriptionsCheck()
setInterval(doUpdate, Number(process.env.POLLING_INTERVAL) * 1000)
setInterval(doAllTrackerOfflineCheck, Number(process.env.POLLING_INTERVAL) * 1000)
setInterval(doTrackerDeadAndSubscriptionsCheck, Number(process.env.POLLING_INTERVAL) * 1000)
