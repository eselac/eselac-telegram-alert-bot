interface TrackerTypeResponse {
    name: string
    battery_voltage_warning?: number
    battery_voltage_critical?: number
}

interface TrackerResponse {
    device_id: string
    battery_voltage: number | null
    internal: boolean
    last_reported: string
    tracker_type: TrackerTypeResponse
    tracker_status: 'active' | 'inactive' | 'missing' | 'decommissioned'
    api_key: string
    mac_adress: string | null
    last_location_reported: string
    lat: number
    lng: number
}

interface LockTypeResponse {
    name: string
    form_factor: string
}

interface LockResponse {
    pk: number
    unlock_key: string
    lock_type: LockTypeResponse
    lock_id: string
}

export interface BikeResponse {
    bike_id: string
    bike_number: string
    state: 'usable' | 'broken' | 'in_repair' | 'missing'
    availability_status: 'disabled' | 'in_use' | 'available'
    internal_note: string
    wiki_link: string | null
    trackers: TrackerResponse[]
    lock: LockResponse
    lat: number
    lng: number
}

export interface DocumentDataResponse {
    id: string,
    collectionId: string,
    parentDocumentId: string,
    title: string,
    fullWidth: boolean,
    emoji: string,
    text: string,
    urlId: string,
    collaborators: Record<string, unknown>[],
    pinned: boolean,
    template: boolean,
    templateId: string,
    revision: number,
    createdAt: string,
    createdBy: Record<string, unknown>[],
    updatedAt: string,
    updatedBy: Record<string, unknown>[],
    publishedAt: string,
    archivedAt: string,
    deletedAt: string
}

export interface DocumentResponse {
    data: DocumentDataResponse | null
}

export interface LockFromBikeNumberResponse {
    success: boolean
    data: {
        lock: LockResponse
    } | null
}

export interface DocumentListResponse {
    data: DocumentDataResponse[] | null
    policies: Record<string, unknown>[]
    pagination: {
        limit: number
        offset: number
    }
}

export interface DocumentUpdateResponse {
    data: DocumentDataResponse | null
    policies: Record<string, unknown>[]
}

export interface LiveLocation {
    bikeNumber: string
    msgId: number
    intervalId: number
    lat: number
    lng: number
    time: moment.Moment
}

export interface BikeSubscription {
    id: number
    lat: number | null
    lng: number | null
}

export interface Rent {
    bikeNumber: string
    finishUrl: string
}

export interface State {
    subscriptions: BikeSubscription[]
    liveLocation: LiveLocation | null
    rents: Rent[]
}

export interface RentStartResponse {
    url: string
    id: number
    bike: {
        bike_number: string
        lock_type: string
    }
    rent_start: string
    rent_end: null
    finish_url: string
    unlock_url: string
}

export interface UnlockResponse {
    success: boolean
    data: {
        unlock_key: string
    }
}
