import dotenv from 'dotenv'
import moment from 'moment-timezone'
import TelegramBot from 'node-telegram-bot-api'
import { StateHandler } from './state.js'
import { BikeResponse, DocumentListResponse, LiveLocation, State } from './types.js'
import { fetchWikiDocumentList, getBikeOrUndefined, handleAPIEvent, handleBikeEvent, sendLiveLocation, sendMessage } from './utils.js'

moment.tz.setDefault('Europe/Berlin')
moment.locale('de-DE')

dotenv.config()

export const bot: TelegramBot = new TelegramBot(process.env.BOT_TOKEN ?? '', { polling: true })
const stateHandler: StateHandler = new StateHandler()

bot.onText(/\/follow_bike \d\d\d\d *$/, async (msg, match) => {
  if (msg.chat.id === Number(process.env.CHAT_ID ?? '')) {
    const currState: State = await stateHandler.getState()

    if (currState.liveLocation != null) {
      await sendMessage(
        process.env.CHAT_ID ?? '',
        'There is already one Live Location Tracking running. Telegram only allows one specific Live Location per user.'
      )
    } else if (match != null) {
      const bike: BikeResponse | undefined = await getBikeOrUndefined(match[0].split(' ')[1])

      if (bike === undefined) {
        await sendMessage(
          process.env.CHAT_ID ?? '',
            `Bike with number ${match[0].split(' ')[1]} not found.`
        )
      } else {
        await sendMessage(
          process.env.CHAT_ID ?? '',
            `Sending of live locations for bike ${match[0].split(' ')[1]} started. Send /stop to stop.`
        )

        const interval = Number(setInterval(sendLiveLocation, 60 * 1000, Number(match[0].split(' ')[1])))

        await sendLiveLocation(Number(match[0].split(' ')[1]), true, interval)
      }
    }
  }
})

bot.onText(/\/stop *.*/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    const currState: State = await stateHandler.getState()

    if (currState.liveLocation == null) {
      await sendMessage(process.env.CHAT_ID ?? '', 'There is no Live Location running.')
    } else {
      await bot.stopMessageLiveLocation({
        chat_id: process.env.CHAT_ID,
        message_id: currState.liveLocation.msgId
      })

      clearInterval(currState.liveLocation.intervalId)

      await stateHandler.stopLiveLocation()

      await sendMessage(process.env.CHAT_ID ?? '', 'Live Location stopped.')
    }
  }
})

bot.onText(/\/ping *.*/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    const currState: State = await stateHandler.getState()

    await sendMessage(process.env.CHAT_ID ?? '', 'I am active in this chat.', {
      reply_to_message_id: msg.message_id
    })

    if (currState.liveLocation != null) {
      const currLoc: LiveLocation = currState.liveLocation
      await sendMessage(
        process.env.CHAT_ID ?? '',
          `The last location of this Live Location was at (${currLoc.lat}, ${currLoc.lng}) at ${currLoc.time.format('dd-MM-yyyy HH:mm')}.`,
          { reply_to_message_id: currLoc.msgId }
      )
    }
  }
})

bot.on('channel_post', async (msg) => {
  if (msg.chat.id === Number(process.env.CHANNEL_ID) && msg.text !== undefined) {
    switch (true) {
      case (/\/inspected \d\d\d\d( *$| .*$)/).test(msg.text):
        await handleBikeEvent('inspection', process.env.CHANNEL_ID, msg)
        break
      case (/\/repaired \d\d\d\d( *$| .*$)/).test(msg.text):
        await handleBikeEvent('repair', process.env.CHANNEL_ID, msg)
        break
      case (/\/change_code +\d\d\d\d +\d{4,}/).test(msg.text):
        await handleAPIEvent('changeLockCode', process.env.CHANNEL_ID, msg)
        break
      case (/\/get_code +\d\d\d\d( *$| .*$)/).test(msg.text):
        await handleAPIEvent('getLockCode', process.env.CHANNEL_ID, msg)
        break
      case (/\/repaired( *$| (?!\d\d\d\d( *$| .*$)$))|\/inspected( *$| (?!\d\d\d\d( *$| .*$)))|\/get_code( *$| (?!\d\d\d\d( *$| .*$)))/).test(msg.text): {
        const sentmsg: TelegramBot.Message | null = (await sendMessage(
          process.env.CHANNEL_ID ?? '',
          'You need to provide a bike ID.'
        ))[0]

        await new Promise(resolve => setTimeout(resolve, 3000))

        if (sentmsg != null) {
          await bot.deleteMessage(
            process.env.CHANNEL_ID ?? '',
            sentmsg.message_id)
        }

        await bot.deleteMessage(
          process.env.CHANNEL_ID ?? '',
          msg.message_id)
      }
        break
      case (/\/change_code( .*$|$)/).test(msg.text): {
        const sentmsg: TelegramBot.Message | null = (await sendMessage(
          process.env.CHANNEL_ID ?? '',
          'You need to provide a bike ID and a new lock code with at least 4 digits.'
        ))[0]

        await new Promise(resolve => setTimeout(resolve, 3000))

        if (sentmsg != null) {
          await bot.deleteMessage(
            process.env.CHANNEL_ID ?? '',
            sentmsg.message_id)
        }

        await bot.deleteMessage(
          process.env.CHANNEL_ID ?? '',
          msg.message_id)
      }
        break

      default:
        break
    }
  }
}
)

bot.onText(/\/repaired \d\d\d\d( *$| .*$)/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    await handleBikeEvent('repair', process.env.CHAT_ID, msg)
  }
})

bot.onText(/\/inspected \d\d\d\d( *$| .*$)/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    await handleBikeEvent('inspection', process.env.CHAT_ID, msg)
  }
})

bot.onText(/(\/repaired( *$| (?!\d\d\d\d( *$| .*$)))|\/inspected( *$| (?!\d\d\d\d( *$| .*$)))|\/follow_bike( *$| (?!\d\d\d\d *$))|\/get_code( *$| (?!\d\d\d\d( *$| .*$)))|\/subscribe( *$| (?!\d\d\d\d *$))|\/unsubscribe( *$| (?!\d\d\d\d *$))|\/rent( *$| (?!\d\d\d\d *$))|\/finish_rent( *$| (?!\d\d\d\d *$)))/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    const sentmsg: TelegramBot.Message | null = (await sendMessage(process.env.CHAT_ID ?? '', 'You need to provide a Bike ID.'))[0]

    await new Promise(resolve => setTimeout(resolve, 3000))

    if (sentmsg != null) { await bot.deleteMessage(process.env.CHAT_ID ?? '', sentmsg.message_id) }
    await bot.deleteMessage(process.env.CHAT_ID ?? '', msg.message_id)
  }
})

bot.onText(/\/change_code( *$|(?! \d\d\d\d +\d{4,}( *$| .*$)))/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    const sentmsg: TelegramBot.Message | null = (await sendMessage(
      process.env.CHAT_ID ?? '',
      'You need to provide a bike ID and a new lock code with at least 4 digits.'
    ))[0]

    await new Promise(resolve => setTimeout(resolve, 3000))

    if (sentmsg != null) {
      await bot.deleteMessage(
        process.env.CHAT_ID ?? '',
        sentmsg.message_id)
    }

    await bot.deleteMessage(
      process.env.CHAT_ID ?? '',
      msg.message_id)
  }
})

bot.onText(/\/change_code +\d\d\d\d +\d{4,}/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    await handleAPIEvent('changeLockCode', process.env.CHAT_ID, msg)
  }
})

bot.onText(/\/get_code +\d\d\d\d( *$| .*$)/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    await handleAPIEvent('getLockCode', process.env.CHAT_ID, msg)
  }
})

bot.onText(/\/get_todos */, async (msg) => { // only in Chat, not Channel
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    const allDocs: DocumentListResponse = await fetchWikiDocumentList('wiki.esel.ac', process.env.WIKI_TOKEN ?? '')

    const toDoDocument = allDocs.data?.filter(d => d.title.toLowerCase().includes('todos'))[0]

    if (toDoDocument !== undefined) {
      await sendMessage(process.env.CHAT_ID ?? '',
        '```markdown\n' +
        toDoDocument.text.replace(/\[(.+)\]\(.*?\)/g, '$1') + '```'
        , { reply_to_message_id: msg.message_id, parse_mode: 'MarkdownV2' })
    }
  }
})

bot.onText(/\/subscribe +(\d\d\d\d)(?: *$| .*$)/, async (msg, match) => {
  if (msg.chat.id === Number(process.env.CHAT_ID) && match != null) {
    if (!(await stateHandler.addSubscription(Number(match[1])))) {
      await sendMessage(process.env.CHAT_ID ?? '', `Das Bike mit der Nummer ${match[1]} ist bereits in der Liste der Subscriptions!`, { reply_to_message_id: msg.message_id })
      return
    }

    const bike: BikeResponse | undefined = await getBikeOrUndefined(match[1])

    if (bike === undefined) {
      const sentmsg: TelegramBot.Message | null = (await sendMessage(
        process.env.CHAT_ID ?? '',
          `Bike with number ${match[1]} not found.`
      ))[0]

      await new Promise(resolve => setTimeout(resolve, 3000))

      if (sentmsg != null) {
        await bot.deleteMessage(
          process.env.CHAT_ID ?? '',
          sentmsg.message_id)
      }

      await bot.deleteMessage(
        process.env.CHAT_ID ?? '',
        msg.message_id)
    } else {
      await stateHandler.addSubscription(Number(match[1]), bike.trackers[0].lat, bike.trackers[0].lng)

      await sendMessage(process.env.CHAT_ID ?? '', `Bike mit der Nummer ${match[1]} der Liste der Subscriptions hinzugefügt. Schreibe \`/unsubscribe ${match[1]}\` zum Beenden. Aktuelle Position:\n(${moment(bike.trackers[0].last_reported).format('HH:mm:ss – DD.MM.YY (zz)')})`, { reply_to_message_id: msg.message_id, parse_mode: 'Markdown' })
      await bot.sendLocation(process.env.CHAT_ID ?? '', bike.trackers[0].lat, bike.trackers[0].lng)
    }
  }
})

bot.onText(/\/unsubscribe +(\d\d\d\d)(?: *$| .*$)/, async (msg, match) => {
  if (match != null) {
    if (!(await stateHandler.removeSubscription(Number(match[1])))) {
      await sendMessage(process.env.CHAT_ID ?? '', `Das Bike mit der Nummer ${match[1]} ist nicht in der Liste der Subscriptions!`, { reply_to_message_id: msg.message_id })
      return
    }

    await stateHandler.removeSubscription(Number(match[1]))

    await sendMessage(process.env.CHAT_ID ?? '', `Bike mit der Nummer ${match[1]} aus der Liste der Subscriptions entfernt.`, { reply_to_message_id: msg.message_id })
  }
})

bot.onText(/\/rent +\d\d\d\d( *$| .*$)/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    await handleAPIEvent('startRent', process.env.CHAT_ID, msg)
  }
})

bot.onText(/\/finish_rent +\d\d\d\d( *$| .*$)/, async (msg) => {
  if (msg.chat.id === Number(process.env.CHAT_ID)) {
    await handleAPIEvent('finishRent', process.env.CHAT_ID, msg)
  }
})
