import moment from 'moment-timezone'
import TelegramBot from 'node-telegram-bot-api'
import { bot } from './commands.js'
import { StateHandler } from './state.js'
import { BikeResponse, DocumentDataResponse, DocumentListResponse, DocumentResponse, DocumentUpdateResponse, LockFromBikeNumberResponse, RentStartResponse, State, UnlockResponse } from './types.js'

moment.tz.setDefault('Europe/Berlin')
moment.locale('de-DE')

const stateHandler = new StateHandler()

export const fetchBikes = async (endpoint: string, token: string): Promise<BikeResponse[]> => {
  const response: Response = await fetch(endpoint + 'maintenance/mapdata', {
    headers: {
      Authorization: 'Token ' + token,
      Accept: 'application/json'
    }
  })

  if (response.ok && response.status === 200) {
    const resp: BikeResponse[] = await response.json() as BikeResponse[]

    return resp
  } else {
    console.error('Error while quering bikes:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return []
  }
}

export const fetchWikiDocument = async (baseurl: string, token: string, shareId: string): Promise<DocumentResponse> => {
  const response: Response = await fetch('https://' + baseurl + '/api/documents.info',
    {
      method: 'POST',
      headers:
            {
              Authorization: 'Bearer ' + token,
              'Content-Type': 'application/json',
              Accept: 'application/json'
            },
      body: JSON.stringify({ shareId })
    })

  if (response.ok && response.status === 200) {
    const resp: DocumentResponse = await response.json() as DocumentResponse

    return resp
  } else {
    console.error('Error while quering wiki document:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return { data: null }
  }
}

export const setOption = (option = 'inspection', text: string, set: string): string => {
  let cellTitle = ''
  switch (option) {
    case 'inspection':
      cellTitle = 'Letzte Inspektion'
      break
    case 'repair':
      cellTitle = 'Letzte Reparatur'
  }

  const searchString: string = getOption(option, text)

  let resText: string = text
  const regex = new RegExp('.*\\| *' + cellTitle + ' *\\| *' + searchString.replaceAll('.', '\\.') + ' *\\|.*')

  resText = resText.replace(regex, '| ' + cellTitle + ' | ' + set + ' |')

  return resText
}

export const getOption = (option = 'inspection', text: string): string => {
  let searchString = ''
  switch (option) {
    case 'inspection':
      searchString = 'Letzte Inspektion'
      break
    case 'repair':
      searchString = 'Letzte Reparatur'
      break
  }

  const tableToEndString: string = text.slice(text.indexOf('|'))
  const tableString: string = tableToEndString.slice(0, tableToEndString.indexOf('#'))

  const resOption: string | undefined = tableString.split('\n').find((e) => e.includes(searchString))?.split('|')[2].trim()

  return resOption ?? ''
}

export const fetchLock = async (endpoint: string, token: string, bikeNr: string): Promise<LockFromBikeNumberResponse> => {
  const response: Response = await fetch(endpoint + 'lock/get_lock_from_bike_number', {
    method: 'POST',
    headers: {
      Authorization: 'Token ' + token,
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    body: JSON.stringify({ bike_number: bikeNr })
  })

  if (response.ok && response.status === 200) {
    const resp: LockFromBikeNumberResponse = await response.json() as LockFromBikeNumberResponse

    return resp
  } else {
    console.error('Error while fetching Lock from Bike number:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return { success: false, data: null }
  }
}

export const changeLockCode = async (endpoint: string, token: string, code: string, lockId: number): Promise<{ success: boolean }> => {
  const response: Response = await fetch(endpoint + 'lock/' + lockId + '/change_code', {
    method: 'POST',
    headers: {
      Authorization: 'Token ' + token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ code })
  })

  if (response.ok && response.status === 200) {
    const resp: { success: boolean } = await response.json() as { success: boolean }

    return resp
  } else {
    console.error('Error while changing Lock code:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return { success: false }
  }
}

export const fetchWikiDocumentList = async (baseurl: string, token: string): Promise<DocumentListResponse> => {
  const response: Response = await fetch('https://' + baseurl + '/api/documents.list',
    {
      method: 'POST',
      headers:
            {
              Authorization: 'Bearer ' + token,
              'Content-Type': 'application/json',
              Accept: 'application/json'
            },
      body: JSON.stringify({ limit: 100 })
    })

  if (response.ok && response.status === 200) {
    const resp: DocumentListResponse = await response.json() as DocumentListResponse

    return resp
  } else {
    console.error('Error while fetching Wiki Document List:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return { data: null, policies: [], pagination: { limit: 100, offset: 0 } }
  }
}

export const updateWikiDocument = async (baseurl: string, token: string, docId: string, replaceText: string): Promise<DocumentUpdateResponse> => {
  const response: Response = await fetch('https://' + baseurl + '/api/documents.update',
    {
      method: 'POST',
      headers:
            {
              Authorization: 'Bearer ' + token,
              'Content-Type': 'application/json',
              Accept: 'application/json'
            },
      body: JSON.stringify({ id: docId, text: replaceText })
    })

  if (response.ok && response.status === 200) {
    const resp: DocumentUpdateResponse = await response.json() as DocumentUpdateResponse

    return resp
  } else {
    console.error('Error while updating Wiki Document:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return { data: null, policies: [] }
  }
}

export const sendLiveLocation = async (bikenr: number, start = false, interval?: number): Promise<void> => {
  const bikes: BikeResponse[] = await fetchBikes(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '')

  const bike: BikeResponse = bikes.filter(e => Number(e.bike_number) === bikenr)[0]

  if (start && interval !== undefined) {
    const msg = await bot.sendLocation(
      process.env.CHAT_ID ?? 0,
      bike.trackers[0].lat,
      bike.trackers[0].lng,
      { live_period: 86400 }
    )

    await stateHandler.updateLiveLocation(String(bikenr), msg.message_id, bike.trackers[0].lat, bike.trackers[0].lng, moment(bike.trackers[0].last_reported), interval)
  } else {
    const currState: State = await stateHandler.getState()

    if (
      currState.liveLocation != null &&
      bike.trackers[0].lat !== currState.liveLocation.lat &&
      bike.trackers[0].lng !== currState.liveLocation.lng
    ) {
      await bot.editMessageLiveLocation(bike.trackers[0].lat, bike.trackers[0].lng, {
        chat_id: process.env.CHAT_ID,
        message_id: currState.liveLocation.msgId
      })

      await stateHandler.updateLiveLocation(String(bikenr), currState.liveLocation.msgId, bike.trackers[0].lat, bike.trackers[0].lng, moment(bike.trackers[0].last_reported), interval)
    }
  }
}

export const handleAPIEvent = async (event: string, chatId: string = process.env.CHAT_ID ?? '', msg: TelegramBot.Message): Promise<void> => {
  const match: string[] | undefined = msg.text?.split(' ')

  if (match !== undefined) {
    switch (event) {
      case 'changeLockCode': {
        const bike: BikeResponse | undefined = await getBikeOrUndefined(match[1])

        if (bike === undefined) {
          const sentmsg: TelegramBot.Message | null = (await sendMessage(
            chatId,
          `Bike with number ${match[1]} not found.`
          ))[0]

          if (sentmsg != null) {
            await new Promise(resolve => setTimeout(resolve, 3000))

            await bot.deleteMessage(
              chatId,
              sentmsg.message_id)

            await bot.deleteMessage(
              chatId,
              msg.message_id)
          }
        } else {
          const lockResponse: LockFromBikeNumberResponse = await fetchLock(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '', bike.bike_number)

          let sentmsg: TelegramBot.Message | null

          if (lockResponse.success && lockResponse.data != null) {
            const lockId: number = lockResponse.data.lock.pk

            const changeResponse: { success: boolean } = await changeLockCode(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '', match[2], lockId)

            if (changeResponse.success) {
              let from = ''

              if ((await bot.getChat(chatId)).type === 'channel') {
                from = msg.author_signature ?? ''
              } else {
                if (msg.from !== undefined) {
                  from = msg.from.first_name
                }
              }

              sentmsg = (await sendMessage(chatId, 'Unlock code for the lock of Bike ' + bike.bike_number + ' successfully updated by ' + from + '.'))[0]
            } else {
              sentmsg = (await sendMessage(chatId, 'There was an error while trying to update the unlock code for the lock of bike ' + bike.bike_number + '.'))[0]
            }
          } else {
            sentmsg = (await sendMessage(chatId, 'There was an error while trying to fetch the lock of bike ' + bike.bike_number + '.'))[0]
          }

          await new Promise(resolve => setTimeout(resolve, 3000))

          if ((await bot.getChat(chatId)).type !== 'channel') {
            await bot.deleteMessage(
              chatId,
              msg.message_id)

            if (sentmsg != null) {
              await bot.deleteMessage(
                chatId,
                sentmsg.message_id)

              if (sentmsg.text !== undefined) {
                await sendMessage(
                  process.env.CHANNEL_ID ?? '',
                  sentmsg.text)
              }
            }
            return
          }

          await bot.deleteMessage(
            chatId,
            msg.message_id)
        }
      }
        break
      case 'getLockCode': {
        const bike: BikeResponse | undefined = await getBikeOrUndefined(match[1])

        if (bike === undefined) {
          const sentmsg: TelegramBot.Message | null = (await sendMessage(
            chatId,
          `Bike with number ${match[1]} not found.`
          ))[0]

          await new Promise(resolve => setTimeout(resolve, 3000))

          if (sentmsg != null) {
            await bot.deleteMessage(
              chatId,
              sentmsg.message_id)
          }

          await bot.deleteMessage(
            chatId,
            msg.message_id)
        } else {
          const lockResponse: LockFromBikeNumberResponse = await fetchLock(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '', bike.bike_number)

          let sentmsg: TelegramBot.Message | null

          if (lockResponse.success && lockResponse.data != null) {
            sentmsg = (await sendMessage(chatId, 'The code of bike ' + bike.bike_number + ' is ' + lockResponse.data.lock.unlock_key + '.'))[0]
          } else {
            sentmsg = (await sendMessage(chatId, 'There was an error while trying to fetch the lock of bike ' + bike.bike_number + '.'))[0]
          }

          await new Promise(resolve => setTimeout(resolve, 3000))

          await bot.deleteMessage(
            chatId,
            msg.message_id)

          if (sentmsg != null) {
            await bot.deleteMessage(
              chatId,
              sentmsg.message_id)
          }
        }
      }
        break
      case 'startRent': {
        const bike: BikeResponse | undefined = await getBikeOrUndefined(match[1])

        if (bike === undefined) {
          const sentmsg: TelegramBot.Message | null = (await sendMessage(
            chatId,
          `Bike with number ${match[1]} not found.`
          ))[0]

          await new Promise(resolve => setTimeout(resolve, 3000))

          if (sentmsg != null) {
            await bot.deleteMessage(
              chatId,
              sentmsg.message_id)
          }

          await bot.deleteMessage(
            chatId,
            msg.message_id)
        } else {
          let sentmsg: TelegramBot.Message | null

          if (!(await stateHandler.rentExists(bike.bike_number))) {
            const startRentResponse: RentStartResponse | null = await startRent(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '', bike.bike_number)

            if (startRentResponse != null) {
              const unlockUrl: string = startRentResponse.unlock_url
              const finishUrl: string = startRentResponse.finish_url

              await stateHandler.addRent(bike.bike_number, finishUrl)

              let from = ''

              if ((await bot.getChat(chatId)).type === 'channel') {
                from = msg.author_signature ?? ''
              } else {
                if (msg.from !== undefined) {
                  from = msg.from.first_name
                }
              }

              sentmsg = (await sendMessage(chatId, 'A rent has been started for bike ' + bike.bike_number + ' by ' + from + '.'))[0]

              await new Promise(resolve => setTimeout(resolve, 3000))

              if ((await bot.getChat(chatId)).type !== 'channel') {
                await bot.deleteMessage(
                  chatId,
                  msg.message_id)

                if (sentmsg != null) {
                  await bot.deleteMessage(
                    chatId,
                    sentmsg.message_id)

                  if (sentmsg.text !== undefined) {
                    await sendMessage(
                      process.env.CHANNEL_ID ?? '',
                      sentmsg.text)
                  }
                }
              }

              const unlockResponse: UnlockResponse | null = await unlock(unlockUrl, process.env.ESEL_TOKEN ?? '')

              if (unlockResponse != null) {
                sentmsg = (await sendMessage(chatId, 'The code of bike ' + bike.bike_number + ' is ' + unlockResponse.data.unlock_key + '.'))[0]
              } else {
                sentmsg = (await sendMessage(chatId, 'There was an error while trying to unlock bike ' + bike.bike_number + '.'))[0]
              }

              await new Promise(resolve => setTimeout(resolve, 3000))

              await bot.deleteMessage(
                chatId,
                msg.message_id)

              if (sentmsg != null) {
                await bot.deleteMessage(
                  chatId,
                  sentmsg.message_id)
              }
            } else {
              sentmsg = (await sendMessage(chatId, 'There was an error while trying to rent bike ' + bike.bike_number + '.'))[0]
            }
          } else {
            sentmsg = (await sendMessage(chatId, 'There already exists a rent for bike ' + bike.bike_number + '.'))[0]

            await new Promise(resolve => setTimeout(resolve, 3000))

            await bot.deleteMessage(
              chatId,
              msg.message_id)

            if (sentmsg != null) {
              await bot.deleteMessage(
                chatId,
                sentmsg.message_id)
            }
          }
        }
      }

        break
      case 'finishRent': {
        const bike: BikeResponse | undefined = await getBikeOrUndefined(match[1])

        if (bike === undefined) {
          const sentmsg: TelegramBot.Message | null = (await sendMessage(
            chatId,
          `Bike with number ${match[1]} not found.`
          ))[0]

          await new Promise(resolve => setTimeout(resolve, 3000))

          if (sentmsg != null) {
            await bot.deleteMessage(
              chatId,
              sentmsg.message_id)
          }

          await bot.deleteMessage(
            chatId,
            msg.message_id)
        } else {
          let sentmsg: TelegramBot.Message | null

          if (await stateHandler.rentExists(bike.bike_number)) {
            const finishRentResponse: { success: boolean } | null = await finishRent(await stateHandler.getRentFinishUrl(bike.bike_number), process.env.ESEL_TOKEN ?? '')

            if (finishRentResponse != null) {
              await stateHandler.removeRent(bike.bike_number)

              sentmsg = (await sendMessage(chatId, 'Rent for bike ' + bike.bike_number + ' successfully finished.'))[0]
            } else {
              sentmsg = (await sendMessage(chatId, 'There was an error while trying to finish the rent for bike ' + bike.bike_number + '.'))[0]
            }

            await new Promise(resolve => setTimeout(resolve, 3000))

            await bot.deleteMessage(
              chatId,
              msg.message_id)

            if (sentmsg != null) {
              await bot.deleteMessage(
                chatId,
                sentmsg.message_id)
            }
          } else {
            sentmsg = (await sendMessage(chatId, 'There is no running rent for bike ' + bike.bike_number + '.'))[0]

            await new Promise(resolve => setTimeout(resolve, 3000))

            await bot.deleteMessage(
              chatId,
              msg.message_id)

            if (sentmsg != null) {
              await bot.deleteMessage(
                chatId,
                sentmsg.message_id)
            }
          }
        }
      }
        break
      default:
        break
    }
  }
}

export const handleBikeEvent = async (event = 'inspection', chatId: TelegramBot.ChatId = process.env.CHAT_ID ?? '', msg: TelegramBot.Message): Promise<void> => {
  const match: string[] | undefined = msg.text?.split(' ')
  const expectedDateFormats: string[] = ['DD.MM.YYYY', 'DD.MM.YY', 'D.M.YY', 'D.M.YYYY', 'DD.M.YY', 'DD.M.YYYY', 'D.MM.YY', 'D.MM.YYYY']
  let validFormat = ''

  if (match !== undefined) {
    if ((event === 'inspection' || event === 'repair')) {
      let found = 0
      for (const m of match) {
        if (match.indexOf(m) < 2 || m === '') {
          continue
        } else {
          found = match.indexOf(m)
          break
        }
      }

      for (const format of expectedDateFormats) {
        if (moment(match[found], format, true).isValid()) {
          validFormat = format
          break
        }
      }

      if (found !== 0 && validFormat === '') {
        const sentmsg: TelegramBot.Message | null = (await sendMessage(chatId, 'The provided Date ' + match[2] + ' is not in a valid Date format. Please conform to DD.MM.YYYY.'))[0]

        await new Promise(resolve => setTimeout(resolve, 3000))

        if (sentmsg != null) {
          await bot.deleteMessage(
            chatId,
            sentmsg.message_id)
        }

        await bot.deleteMessage(
          chatId,
          msg.message_id)
        return
      }
    }

    const bike: BikeResponse | undefined = await getBikeOrUndefined(match[1])

    if (bike === undefined) {
      const sentmsg: TelegramBot.Message | null = (await sendMessage(
        chatId,
      `Bike with number ${match[1]} not found.`
      ))[0]

      await new Promise(resolve => setTimeout(resolve, 3000))

      if (sentmsg != null) {
        await bot.deleteMessage(
          chatId,
          sentmsg.message_id)
      }

      await bot.deleteMessage(
        chatId,
        msg.message_id)
    } else if (!bike.wiki_link) {
      const sentmsg: TelegramBot.Message | null = (await sendMessage(
        chatId,
      `Bike with number ${match[1]} has no wiki link provided in the Backend.`
      ))[0]

      await new Promise(resolve => setTimeout(resolve, 3000))

      if (sentmsg != null) {
        await bot.deleteMessage(
          chatId,
          sentmsg.message_id)
      }

      await bot.deleteMessage(
        chatId,
        msg.message_id)
    } else {
      const baseurl: string = bike.wiki_link.split('//')[1].split('/')[0]

      const bikeDocs: DocumentListResponse = await fetchWikiDocumentList(baseurl, process.env.WIKI_TOKEN ?? '')

      const relevantBikeDoc: DocumentDataResponse | undefined = bikeDocs.data?.find(b => b.title.includes(bike.bike_number))

      if (relevantBikeDoc !== undefined) {
        const relevantBikeDocId: string = relevantBikeDoc.id
        const relevantBikeDocText: string = relevantBikeDoc.text

        let newValue = ''

        if (event === 'inspection' || event === 'repair') {
          let found = 0
          for (const m of match) {
            if (match.indexOf(m) < 2 || m === '') {
              continue
            } else {
              found = match.indexOf(m)
              break
            }
          }

          for (const format of expectedDateFormats) {
            if (moment(match[found], format, true).isValid()) {
              validFormat = format
              break
            }
          }

          newValue = found !== 0 ? moment(match[found], validFormat, true).format('DD.MM.YYYY') : moment().format('DD.MM.YYYY')
        } else {
          newValue = match[2]
        }

        const replaceText: string = setOption(event, relevantBikeDocText, newValue)

        const response: DocumentUpdateResponse = await updateWikiDocument(baseurl, process.env.WIKI_TOKEN ?? '', relevantBikeDocId, replaceText)

        let sentmsg: TelegramBot.Message | null

        if (response.data != null) {
          let from = ''

          if ((await bot.getChat(chatId)).type === 'channel') {
            from = msg.author_signature ?? ''
          } else {
            if (msg.from !== undefined) {
              from = msg.from.first_name
            }
          }
          if (event === 'inspection' || event === 'repair') {
            sentmsg = (await sendMessage(chatId, 'Last ' + event.replace(event.charAt(0), event.charAt(0).toLocaleUpperCase()) + ' for Bike ' + bike.bike_number + ' successfully updated to ' + newValue + ' by ' + from + '.'))[0]
          } else {
            sentmsg = (await sendMessage(chatId, 'Option ' + event.replace(event.charAt(0), event.charAt(0).toLocaleUpperCase()) + ' for bike ' + bike.bike_number + ' successfully updated to ' + newValue + ' by ' + from + '.'))[0]
          }
        } else {
          sentmsg = (await sendMessage(chatId, 'There was an error while trying to update Bike ' + bike.bike_number + '.'))[0]
        }

        await new Promise(resolve => setTimeout(resolve, 3000))

        if ((await bot.getChat(chatId)).type !== 'channel') {
          await bot.deleteMessage(
            chatId,
            msg.message_id)

          if (sentmsg != null) {
            await bot.deleteMessage(
              chatId,
              sentmsg.message_id)

            if (sentmsg.text !== undefined) {
              await sendMessage(
                process.env.CHANNEL_ID ?? '',
                sentmsg.text)
            }
          }
          return
        }

        await bot.deleteMessage(
          chatId,
          msg.message_id)
      }
    }
  }
}

export const sendMessage = async (id: TelegramBot.ChatId, msg: string, args: Record<string, unknown> = {}): Promise<(TelegramBot.Message | null)[]> => {
  const splitMessage: string[] = msg.split('\n').map(l => `${l}\n`)
  const messages: string[] = []

  // check message max length
  if (msg.length > 4095) {
    let message = ''
    for (const line of splitMessage) {
      if (msg.includes('```')) { // consider markdown code messages
        if (message.length + 3 + line.length > 4095) {
          messages.push(message + '```')
          message = '```markdown\n' + line
        } else {
          message += line
        }
      } else {
        if (message.length + line.length > 4095) {
          messages.push(message)
          message = line
        } else {
          message += line
        }
      }
    }
    messages.push(message)
  } else {
    messages.push(msg)
  }

  try {
    let first: TelegramBot.Message | null = null
    for (const message of messages) {
      if (messages.indexOf(message) === 0) {
        first = await bot.sendMessage(id, message, args)
      } else {
        await bot.sendMessage(id, message, args)
      }
    }
    return [first]
  } catch (e) {
    console.error('Error while sending message', e)
    return [null]
  }
}

export const getBikeOrUndefined = async (bikeNumber: string): Promise<BikeResponse | undefined> => {
  const bikes: BikeResponse[] = await fetchBikes(process.env.ENDPOINT ?? '', process.env.ESEL_TOKEN ?? '')

  const bike: BikeResponse | undefined = bikes.find(e => e.bike_number === bikeNumber)

  return bike
}

export const startRent = async (endpoint: string, token: string, bikeNumber: string): Promise<RentStartResponse | null> => {
  const response: Response = await fetch(endpoint + 'rent_admin', {
    method: 'POST',
    headers: {
      Authorization: 'Token ' + token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ bike: bikeNumber })
  })

  if (response.ok && response.status === 201) {
    const resp: RentStartResponse = await response.json() as RentStartResponse

    return resp
  } else {
    console.error('Error while starting rent', response.statusText, await response.text())

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return null
  }
}

export const unlock = async (unlockUrl: string, token: string): Promise<UnlockResponse | null> => {
  const response: Response = await fetch(unlockUrl, {
    method: 'POST',
    headers: {
      Authorization: 'Token ' + token,
      'Content-Type': 'application/json'
    }
  })

  if (response.ok && response.status === 200) {
    const resp: UnlockResponse = await response.json() as UnlockResponse

    return resp
  } else {
    console.error('Error while unlocking bike', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return null
  }
}

export const finishRent = async (finishUrl: string, token: string): Promise<{success: boolean} | null > => {
  const response: Response = await fetch(finishUrl, {
    method: 'POST',
    headers: {
      Authorization: 'Token ' + token,
      'Content-Type': 'application/json'
    }
  })

  if (response.ok && response.status === 200) {
    const resp: {success: boolean} = await response.json() as {success: boolean}

    return resp
  } else {
    console.error('Error while finishing rent', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return null
  }
}
