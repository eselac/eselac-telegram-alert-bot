import * as fsSync from 'fs'
import * as fs from 'fs/promises'
import { BikeSubscription, LiveLocation, State } from './types.js'

export class StateHandler {
  path: string
  initialData: State

  constructor () {
    this.path = './state'
    this.initialData = {
      liveLocation: null,
      subscriptions: [],
      rents: []
    }
  }

  private createFile = async (): Promise<void> => {
    await fs.writeFile(this.path, JSON.stringify(this.initialData as unknown))
  }

  private changeSubscriptions = async (type: 'subscribe' | 'unsubscribe', id: number, lat: number | null = null, lng: number | null = null): Promise<boolean> => {
    const currState: State = await this.getState()

    switch (type) {
      case 'subscribe':
        if (currState.subscriptions.find(s => s.id === id) !== undefined) {
          return false
        } else {
          if (lat != null && lng != null) {
            await fs.writeFile(this.path, JSON.stringify({
              liveLocation: currState.liveLocation,
              subscriptions: currState.subscriptions.concat({ id, lat, lng }),
              rents: currState.rents
            }))
          }
          return true
        }

      case 'unsubscribe':
        if (currState.subscriptions.find(s => s.id === id) === undefined) {
          return false
        } else {
          await fs.writeFile(this.path, JSON.stringify({
            liveLocation: currState.liveLocation,
            subscriptions: currState.subscriptions.filter((s) => s.id !== id),
            rents: currState.rents
          }))
          return true
        }
    }
  }

  private changeRents = async (type: 'add' | 'remove', bikeNumber: string, finishUrl?: string): Promise<void> => {
    const currState: State = await this.getState()

    switch (type) {
      case 'add':
        await fs.writeFile(this.path, JSON.stringify({
          liveLocation: currState.liveLocation,
          subscriptions: currState.subscriptions,
          rents: currState.rents.concat({ bikeNumber, finishUrl: finishUrl ?? '' })
        }))
        break
      case 'remove':
        await fs.writeFile(this.path, JSON.stringify({
          liveLocation: currState.liveLocation,
          subscriptions: currState.subscriptions,
          rents: currState.rents.filter(r => r.bikeNumber !== bikeNumber)
        }))
    }
  }

  public getState = async (): Promise<State> => {
    try {
      fsSync.statSync(this.path)
    } catch (e) {
      await this.createFile()
      return this.initialData
    }

    const state: State = JSON.parse((await fs.readFile(this.path)).toString()) as State

    return state
  }

  public stopLiveLocation = async (): Promise<void> => {
    const currState: State = await this.getState()

    await fs.writeFile(this.path, JSON.stringify({
      liveLocation: null,
      subscriptions: currState.subscriptions,
      rents: currState.rents
    }))
  }

  public addSubscription = async (id: number, lat: number | null = null, lng: number | null = null): Promise<boolean> => {
    return this.changeSubscriptions('subscribe', id, lat, lng)
  }

  public removeSubscription = async (id: number): Promise<boolean> => {
    return this.changeSubscriptions('unsubscribe', id)
  }

  public addRent = async (bikeNumber: string, finishUrl: string): Promise<void> => {
    if (!(await this.rentExists(bikeNumber))) {
      await this.changeRents('add', bikeNumber, finishUrl)
    }
  }

  public removeRent = async (bikeNumber: string): Promise<void> => {
    if (await this.rentExists(bikeNumber)) {
      await this.changeRents('remove', bikeNumber)
    }
  }

  public rentExists = async (bikeNumber: string): Promise<boolean> => {
    const currState: State = await this.getState()

    return currState.rents.find(r => r.bikeNumber === bikeNumber) !== undefined
  }

  public getRentFinishUrl = async (bikeNumber: string): Promise<string> => {
    const currState: State = await this.getState()

    return currState.rents.find(r => r.bikeNumber === bikeNumber)?.finishUrl ?? ''
  }

  public updateSubscription = async (id: number, lat: number, lng: number): Promise<void> => {
    const currState: State = await this.getState()
    const relevantSub: BikeSubscription | undefined = currState.subscriptions.find((s) => s.id === id)

    if (relevantSub !== undefined) {
      const newSubs: BikeSubscription[] = [
        ...currState.subscriptions.filter((s) => s.id !== id),
        { id, lat, lng }]

      await fs.writeFile(this.path, JSON.stringify({
        liveLocation: currState.liveLocation,
        subscriptions: newSubs,
        rents: currState.rents
      }))
    }
  }

  public getLiveLocation = async (): Promise<LiveLocation | null> => {
    const currState: State = await this.getState()

    return currState.liveLocation
  }

  public updateLiveLocation = async (bikeNumber: string, msgId: number, lat: number, lng: number, time: moment.Moment, intervalId: number | null = null): Promise<void> => {
    const currState: State = await this.getState()

    if (intervalId != null) {
      await fs.writeFile(this.path, JSON.stringify({
        liveLocation: { bikeNumber, msgId, lat, lng, time, intervalId },
        subscriptions: currState.subscriptions,
        rents: currState.rents
      }))
    } else {
      await fs.writeFile(this.path, JSON.stringify({
        liveLocation: { msgId, lat, lng, time, intervalId: currState.liveLocation?.intervalId },
        subscriptions: currState.subscriptions,
        rents: currState.rents
      }))
    }
  }
}
